# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20171007154803) do

  create_table "tbl_Applicant_EducationalBackground_Schools", primary_key: "applicantEducationalBackgroundId", id: :integer, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.integer "educationalBackgroundId", null: false
    t.integer "schoolId", null: false
    t.integer "applicantId", null: false
    t.integer "createdBy", default: 0, null: false
    t.timestamp "dateCreated", default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.integer "modifiedBy", default: 0, null: false
    t.timestamp "dateModified", default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.binary "active", limit: 1, default: "b'1'", null: false
    t.index ["applicantId"], name: "fk_tbl_Applicant_EducationalBackground_Schools_tbl_Applican_idx"
    t.index ["educationalBackgroundId"], name: "fk_tbl_Applicant_EducationalBackground_Schools_tbl_Educatio_idx"
    t.index ["schoolId"], name: "fk_tbl_Applicant_EducationalBackground_Schools_tbl_Schools1_idx"
  end

  create_table "tbl_Applicant_JobApplications", primary_key: "applicantJobApplications", id: :integer, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.integer "applicantId", null: false
    t.integer "jobApplicationId", null: false
    t.integer "createdBy", default: 0, null: false
    t.timestamp "dateCreated", default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.integer "modifiedBy", default: 0, null: false
    t.timestamp "dateModified", default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.binary "active", limit: 1, default: "b'1'", null: false
    t.index ["applicantId"], name: "fk_tbl_Applicant_JobApplications_tbl_Applicants1_idx"
    t.index ["jobApplicationId"], name: "fk_tbl_Applicant_JobApplications_tbl_JobApplications1_idx"
  end

  create_table "tbl_Applicants", primary_key: "applicantId", id: :integer, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string "applicantNumber", limit: 45, null: false, collation: "utf8_general_ci"
    t.string "lastname", limit: 250, null: false, collation: "utf8_general_ci"
    t.string "middlename", limit: 250, null: false, collation: "utf8_general_ci"
    t.string "gender", limit: 45, null: false
    t.date "birthdate"
    t.string "birthPlace", limit: 500, collation: "utf8_general_ci"
    t.string "currentAddress", limit: 500, collation: "utf8_general_ci"
    t.string "permanentAddress", limit: 500, collation: "utf8_general_ci"
    t.string "phoneNumber", limit: 20, collation: "utf8_general_ci"
    t.string "mobileNumber", limit: 15, collation: "utf8_general_ci"
    t.integer "postalCode"
    t.integer "civilStatusId", null: false
    t.string "height", limit: 45
    t.string "weight", limit: 45
    t.string "sssNum", limit: 45
    t.string "tinNum", limit: 45
    t.string "phicNum", limit: 45
    t.string "hdmfNum", limit: 45
    t.string "nickName", limit: 45
    t.string "emailAddress", limit: 250, collation: "utf8_general_ci"
    t.integer "citizenshipId", null: false
    t.integer "applicationModeId", null: false
    t.integer "applicationSourceId", null: false
    t.integer "createdBy", default: 0, null: false
    t.timestamp "dateCreated", default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.integer "modifiedBy", default: 0, null: false
    t.timestamp "dateModified", default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.binary "active", limit: 1, default: "b'1'", null: false
    t.index ["applicationModeId"], name: "fk_tbl_Applicants_tbl_ApplicationMode1_idx"
    t.index ["applicationSourceId"], name: "fk_tbl_Applicants_tbl_ApplicationSource1_idx"
    t.index ["citizenshipId"], name: "fk_tbl_Applicants_tbl_Citizenships1_idx"
    t.index ["civilStatusId"], name: "fk_tbl_Applicants_tbl_CivilStatuses1_idx"
  end

  create_table "tbl_ApplicationMode", primary_key: "applicationModeId", id: :integer, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string "applicationMode", limit: 45, null: false, collation: "utf8_general_ci"
    t.string "description", limit: 45
    t.integer "createdBy", default: 0, null: false
    t.timestamp "dateCreated", default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.integer "modifiedBy", default: 0, null: false
    t.timestamp "dateModified", default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.binary "active", limit: 1, default: "b'1'", null: false
  end

  create_table "tbl_ApplicationSource", primary_key: "applicationSourceId", id: :integer, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string "applicationSource", limit: 45, null: false
    t.string "description", limit: 500, collation: "utf8_general_ci"
    t.integer "createdBy", default: 0, null: false
    t.timestamp "dateCreated", default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.integer "modifiedBy", default: 0, null: false
    t.timestamp "dateModified", default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.binary "active", limit: 1, default: "b'1'", null: false
  end

  create_table "tbl_Citizenships", primary_key: "citizenshipId", id: :integer, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string "citizenship", limit: 100, null: false, collation: "utf8_general_ci"
    t.string "country", limit: 100, null: false, collation: "utf8_general_ci"
    t.integer "createdBy", default: 0, null: false
    t.timestamp "dateCreated", default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.integer "modifiedBy", default: 0, null: false
    t.timestamp "dateModified", default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.binary "active", limit: 1, default: "b'1'", null: false
  end

  create_table "tbl_CivilStatuses", primary_key: "civilStatusId", id: :integer, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string "civilStatus", limit: 45, null: false
    t.string "civilStatusDescription", limit: 45
    t.integer "createdBy", default: 0, null: false
    t.timestamp "dateCreated", default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.integer "modifiedBy", default: 0, null: false
    t.timestamp "dateModified", default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.binary "active", limit: 1, default: "b'1'", null: false
  end

  create_table "tbl_EducationalBackground", primary_key: "educationalBackgroundId", id: :integer, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string "educationalBackground", limit: 45, null: false
    t.integer "createdBy", default: 0, null: false
    t.timestamp "dateCreated", default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.integer "modifiedBy", default: 0, null: false
    t.timestamp "dateModified", default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.binary "active", limit: 1, default: "b'1'", null: false
  end

  create_table "tbl_EmploymentRequirements", primary_key: "requirementId", id: :integer, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string "requirement", limit: 500, null: false
    t.string "description", limit: 500, collation: "utf8_general_ci"
    t.binary "isRequired", limit: 1, default: "b'0'"
    t.integer "createdBy", default: 0, null: false
    t.timestamp "dateCreated", default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.integer "modifiedBy", default: 0, null: false
    t.timestamp "dateModified", default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.binary "active", limit: 1, default: "b'1'", null: false
  end

  create_table "tbl_JobApplication_EmploymentRequirements", primary_key: "jobApplicationEmploymentRequirementId", id: :integer, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.integer "requirementId", null: false
    t.integer "jobApplicationId", null: false
    t.integer "createdBy", default: 0, null: false
    t.timestamp "dateCreated", default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.integer "modifiedBy", default: 0, null: false
    t.timestamp "dateModified", default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.binary "active", limit: 1, default: "b'1'", null: false
    t.index ["jobApplicationId"], name: "fk_tbl_JobApplication_EmploymentRequirements_tbl_JobApplica_idx"
    t.index ["requirementId"], name: "fk_tbl_JobApplication_EmploymentRequirements_tbl_Employment_idx"
  end

  create_table "tbl_JobApplications", primary_key: "jobApplicationId", id: :integer, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string "jobApplication", limit: 45, null: false, collation: "utf8_general_ci"
    t.string "jobApplicationDescription", limit: 1500, collation: "utf8_general_ci"
    t.integer "createdBy", default: 0, null: false
    t.timestamp "dateCreated", default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.integer "modifiedBy", default: 0, null: false
    t.timestamp "dateModified", default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.binary "active", limit: 1, default: "b'1'", null: false
  end

  create_table "tbl_Schools", primary_key: "schoolId", id: :integer, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string "school", limit: 45, null: false
    t.integer "createdBy", default: 0, null: false
    t.timestamp "dateCreated", default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.integer "modifiedBy", default: 0, null: false
    t.timestamp "dateModified", default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.binary "active", limit: 1, default: "b'1'", null: false
  end

  create_table "tbl_claim_types", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string "claim_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "tbl_claims_and_reimbursements", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.integer "userid"
    t.string "application_type", limit: 24
    t.string "cr_type", limit: 64
    t.text "purpose_of_expense"
    t.string "date_filed", limit: 32
    t.string "period_start", limit: 32
    t.string "period_end", limit: 32
    t.string "cr_status", limit: 24
    t.datetime "created_at", default: "2017-10-10 10:26:33", null: false
    t.datetime "updated_at", default: "2017-10-10 10:26:33", null: false
  end

  create_table "tbl_company_holidays", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string "description"
    t.date "holiday_date"
    t.datetime "created_at", default: "2017-10-10 10:26:32", null: false
    t.datetime "updated_at", default: "2017-10-10 10:26:32", null: false
  end

  create_table "tbl_expense_categories", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string "category_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "tbl_expenses", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.integer "cr_id"
    t.string "expense_date", limit: 32
    t.text "description"
    t.string "category", limit: 64
    t.float "cost", limit: 24
    t.datetime "created_at", default: "2017-10-10 10:26:35", null: false
    t.datetime "updated_at", default: "2017-10-10 10:26:35", null: false
  end

  create_table "tbl_finance_account_types", id: :integer, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string "type", limit: 32, null: false
    t.text "description"
    t.timestamp "created_at", default: -> { "CURRENT_TIMESTAMP" }
    t.timestamp "updated_at", default: -> { "CURRENT_TIMESTAMP" }
    t.integer "status_flag", limit: 1, default: 1
  end

  create_table "tbl_finance_coa", primary_key: ["id", "account_no"], force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.integer "id", null: false
    t.integer "account_no", limit: 2, null: false
    t.string "account_title", limit: 64, null: false
    t.string "to_increase", limit: 6, null: false
    t.integer "account_id", null: false
    t.text "description"
    t.timestamp "created_at", default: -> { "CURRENT_TIMESTAMP" }
    t.timestamp "updated_at", default: -> { "CURRENT_TIMESTAMP" }
    t.integer "status_flag", limit: 1, default: 1
    t.index ["account_id"], name: "account_type"
  end

  create_table "tbl_finance_disburse_acc_payable", primary_key: "invoice_code", id: :string, limit: 50, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.integer "ap_id", null: false
    t.integer "coa_code", null: false
    t.datetime "ap_generated_date", null: false
    t.text "ap_remarks"
    t.text "ap_document_link"
    t.integer "user_id"
    t.timestamp "created_at", default: -> { "CURRENT_TIMESTAMP" }
    t.timestamp "updated_at", default: -> { "CURRENT_TIMESTAMP" }
    t.integer "status_flag", limit: 1, default: 1
    t.index ["ap_id"], name: "ap_id", unique: true
  end

  create_table "tbl_finance_disburse_account_asset", id: false, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.integer "id", null: false
    t.string "invoice_code", limit: 50, null: false
    t.integer "coa_account_number", null: false
    t.string "debit", limit: 50
    t.string "credit", limit: 50
    t.datetime "transaction_date", null: false
    t.timestamp "created_at", default: -> { "CURRENT_TIMESTAMP" }
    t.timestamp "updated_at", default: -> { "CURRENT_TIMESTAMP" }
    t.integer "status_flag", limit: 1, default: 1
    t.index ["id"], name: "id", unique: true
  end

  create_table "tbl_finance_disburse_account_equity", id: false, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.integer "id", null: false
    t.string "invoice_code", limit: 50, null: false
    t.integer "coa_account_number", null: false
    t.string "debit", limit: 50
    t.string "credit", limit: 50
    t.datetime "transaction_date", null: false
    t.timestamp "created_at", default: -> { "CURRENT_TIMESTAMP" }
    t.timestamp "updated_at", default: -> { "CURRENT_TIMESTAMP" }
    t.integer "status_flag", limit: 1, default: 1
    t.index ["id"], name: "id", unique: true
  end

  create_table "tbl_finance_disburse_account_expense", id: false, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.integer "id", null: false
    t.string "invoice_code", limit: 50, null: false
    t.integer "coa_account_number", null: false
    t.string "debit", limit: 50
    t.string "credit", limit: 50
    t.datetime "transaction_date", null: false
    t.timestamp "created_at", default: -> { "CURRENT_TIMESTAMP" }
    t.timestamp "updated_at", default: -> { "CURRENT_TIMESTAMP" }
    t.integer "status_flag", limit: 1, default: 1
    t.index ["id"], name: "id", unique: true
  end

  create_table "tbl_finance_disburse_account_liability", id: false, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.integer "id", null: false
    t.string "invoice_code", limit: 50, null: false
    t.integer "coa_account_number", null: false
    t.string "debit", limit: 50
    t.string "credit", limit: 50
    t.datetime "transaction_date", null: false
    t.timestamp "created_at", default: -> { "CURRENT_TIMESTAMP" }
    t.timestamp "updated_at", default: -> { "CURRENT_TIMESTAMP" }
    t.integer "status_flag", limit: 1, default: 1
    t.index ["id"], name: "id", unique: true
  end

  create_table "tbl_finance_disburse_account_revenue", id: false, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.integer "id", null: false
    t.string "invoice_code", limit: 50, null: false
    t.integer "coa_account_number", null: false
    t.string "debit", limit: 50
    t.string "credit", limit: 50
    t.datetime "transaction_date", null: false
    t.timestamp "created_at", default: -> { "CURRENT_TIMESTAMP" }
    t.timestamp "updated_at", default: -> { "CURRENT_TIMESTAMP" }
    t.integer "status_flag", limit: 1, default: 1
    t.index ["id"], name: "id", unique: true
  end

  create_table "tbl_finance_disburse_bank", primary_key: "bank_code", id: :string, limit: 50, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.integer "id", null: false
    t.text "bank_name", null: false
    t.string "bank_account_type", limit: 250
    t.timestamp "created_at", default: -> { "CURRENT_TIMESTAMP" }
    t.timestamp "updated_at", default: -> { "CURRENT_TIMESTAMP" }
    t.integer "status_flag", limit: 1, default: 1
    t.index ["id"], name: "id", unique: true
  end

  create_table "tbl_finance_disburse_check_register", primary_key: "check_acc_number", id: :string, limit: 100, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.integer "id", null: false
    t.string "bank_code", limit: 50, null: false
    t.timestamp "created_at", default: -> { "CURRENT_TIMESTAMP" }
    t.timestamp "updated_at", default: -> { "CURRENT_TIMESTAMP" }
    t.integer "status_flag", limit: 1, default: 1
    t.index ["id"], name: "id", unique: true
  end

  create_table "tbl_finance_disburse_coa", primary_key: "coa_account_number", id: :integer, default: nil, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.integer "coa_id", null: false
    t.text "coa_account_description", null: false
    t.string "coa_account_type", limit: 500
    t.string "coa_account_norm", limit: 500
    t.timestamp "created_at", default: -> { "CURRENT_TIMESTAMP" }
    t.timestamp "updated_at", default: -> { "CURRENT_TIMESTAMP" }
    t.integer "status_flag", limit: 1, default: 1
    t.index ["coa_id"], name: "coa_id", unique: true
  end

  create_table "tbl_finance_disburse_department", primary_key: "dept_id", id: :integer, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string "dept_name", limit: 500, null: false
    t.timestamp "created_at", default: -> { "CURRENT_TIMESTAMP" }
    t.timestamp "updated_at", default: -> { "CURRENT_TIMESTAMP" }
    t.integer "status_flag", limit: 1, default: 1
  end

  create_table "tbl_finance_disburse_disbursement", primary_key: "invoice_code", id: :string, limit: 50, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.integer "id", null: false
    t.integer "coa_code", null: false
    t.datetime "disburse_generated_date", null: false
    t.text "disburse_remarks"
    t.text "disburse_document_link"
    t.integer "user_id"
    t.string "bank_code", limit: 50, null: false
    t.string "check_number", limit: 100, null: false
    t.string "amount", limit: 50
    t.timestamp "created_at", default: -> { "CURRENT_TIMESTAMP" }
    t.timestamp "updated_at", default: -> { "CURRENT_TIMESTAMP" }
    t.integer "status_flag", limit: 1, default: 1
    t.index ["check_number"], name: "check_number", unique: true
    t.index ["id"], name: "id", unique: true
  end

  create_table "tbl_finance_disburse_invoice_register", primary_key: "invoice_code", id: :string, limit: 50, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.integer "invoice_id", null: false
    t.integer "coa_code", null: false
    t.integer "dept_id", null: false
    t.string "amount", limit: 50, null: false
    t.date "date", null: false
    t.date "due_date", null: false
    t.integer "payment_id", null: false
    t.integer "vendor_id", null: false
    t.string "memo_id", limit: 50, null: false
    t.integer "user_id"
    t.timestamp "created_at", default: -> { "CURRENT_TIMESTAMP" }
    t.timestamp "updated_at", default: -> { "CURRENT_TIMESTAMP" }
    t.integer "status_flag", limit: 1, default: 1
    t.index ["invoice_id"], name: "invoice_id", unique: true
  end

  create_table "tbl_finance_disburse_memo", primary_key: "memo_id", id: :string, limit: 50, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.text "memo"
    t.timestamp "created_at", default: -> { "CURRENT_TIMESTAMP" }
    t.timestamp "updated_at", default: -> { "CURRENT_TIMESTAMP" }
    t.integer "status_flag", limit: 1, default: 1
  end

  create_table "tbl_finance_disburse_payment_type", primary_key: "payment_id", id: :integer, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string "payment_name", limit: 500, null: false
    t.timestamp "created_at", default: -> { "CURRENT_TIMESTAMP" }
    t.timestamp "updated_at", default: -> { "CURRENT_TIMESTAMP" }
    t.integer "status_flag", limit: 1, default: 1
  end

  create_table "tbl_finance_disburse_vendor", primary_key: "vendor_id", id: :integer, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string "vendor_name", limit: 500, null: false
    t.timestamp "created_at", default: -> { "CURRENT_TIMESTAMP" }
    t.timestamp "updated_at", default: -> { "CURRENT_TIMESTAMP" }
    t.integer "status_flag", limit: 1, default: 1
  end

  create_table "tbl_leave_applications", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.integer "userid"
    t.integer "leave_type_id"
    t.string "reason_for_leave"
    t.string "leave_status", limit: 32
    t.string "reason_for_cancelation"
    t.string "reason_for_disapproval"
    t.string "leave_filed_date", limit: 32
    t.string "leave_start_date", limit: 32
    t.string "leave_end_date", limit: 32
    t.datetime "created_at", default: "2017-10-10 10:26:34", null: false
    t.datetime "updated_at", default: "2017-10-10 10:26:34", null: false
  end

  create_table "tbl_leave_types", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string "leave_type"
    t.string "leaves_per_user"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "tbl_leaves", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.integer "userid"
    t.string "leave_type_id"
    t.integer "remaining_leaves"
    t.datetime "created_at", default: "2017-10-10 10:26:33", null: false
    t.datetime "updated_at", default: "2017-10-10 10:26:33", null: false
  end

  create_table "tbl_module_access", id: :integer, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.integer "module_id", null: false
    t.integer "userid", null: false
    t.timestamp "created_at", default: -> { "CURRENT_TIMESTAMP" }
    t.timestamp "updated_at", default: -> { "CURRENT_TIMESTAMP" }
    t.integer "status_flag", limit: 1, default: 1
    t.index ["module_id"], name: "const_module_access"
    t.index ["userid"], name: "const_userid"
  end

  create_table "tbl_modules", id: :integer, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string "name", limit: 32, null: false
    t.text "description"
    t.timestamp "created_at", default: -> { "CURRENT_TIMESTAMP" }
    t.timestamp "updated_at", default: -> { "CURRENT_TIMESTAMP" }
    t.integer "status_flag", limit: 1, default: 1
  end

  create_table "tbl_reimbursement_types", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string "reimbursement_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "tbl_shifts_and_schedules", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.integer "userid"
    t.string "start_date", limit: 32
    t.string "end_date", limit: 32
    t.string "start_time", limit: 24
    t.string "end_time", limit: 24
    t.string "days"
    t.datetime "created_at", default: "2017-10-10 10:26:35", null: false
    t.datetime "updated_at", default: "2017-10-10 10:26:35", null: false
  end

  create_table "tbl_timesheets", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.integer "userid"
    t.date "date"
    t.datetime "start_time"
    t.datetime "end_time"
    t.float "regular_hours", limit: 24, default: 0.0
    t.integer "timesheet_status"
    t.float "overtime_hours", limit: 24, default: 0.0
    t.boolean "overtime_approved", default: false
    t.datetime "created_at", default: "2017-10-10 08:37:27", null: false
    t.datetime "updated_at", default: "2017-10-10 08:37:27", null: false
  end

  create_table "tbl_user", primary_key: ["id", "username"], force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.integer "id", null: false
    t.string "username", limit: 15, null: false
    t.string "password", limit: 32
    t.datetime "last_logged_in"
    t.datetime "last_activity"
    t.string "user_ip", limit: 32
    t.boolean "is_admin"
    t.timestamp "created_at", default: -> { "CURRENT_TIMESTAMP" }
    t.timestamp "updated_at", default: -> { "CURRENT_TIMESTAMP" }
    t.integer "status_flag", limit: 1, default: 1
    t.integer "tokken_status", limit: 1, default: 0
    t.string "login_tokken", limit: 32
  end

  add_foreign_key "tbl_Applicant_EducationalBackground_Schools", "tbl_Applicants", column: "applicantId", primary_key: "applicantId", name: "fk_tbl_Applicant_EducationalBackground_Schools_tbl_Applicants1"
  add_foreign_key "tbl_Applicant_EducationalBackground_Schools", "tbl_EducationalBackground", column: "educationalBackgroundId", primary_key: "educationalBackgroundId", name: "fk_tbl_Applicant_EducationalBackground_Schools_tbl_Educationa1"
  add_foreign_key "tbl_Applicant_EducationalBackground_Schools", "tbl_Schools", column: "schoolId", primary_key: "schoolId", name: "fk_tbl_Applicant_EducationalBackground_Schools_tbl_Schools1"
  add_foreign_key "tbl_Applicant_JobApplications", "tbl_Applicants", column: "applicantId", primary_key: "applicantId", name: "fk_tbl_Applicant_JobApplications_tbl_Applicants1"
  add_foreign_key "tbl_Applicant_JobApplications", "tbl_JobApplications", column: "jobApplicationId", primary_key: "jobApplicationId", name: "fk_tbl_Applicant_JobApplications_tbl_JobApplications1"
  add_foreign_key "tbl_Applicants", "tbl_ApplicationMode", column: "applicationModeId", primary_key: "applicationModeId", name: "fk_tbl_Applicants_tbl_ApplicationMode1"
  add_foreign_key "tbl_Applicants", "tbl_ApplicationSource", column: "applicationSourceId", primary_key: "applicationSourceId", name: "fk_tbl_Applicants_tbl_ApplicationSource1"
  add_foreign_key "tbl_Applicants", "tbl_Citizenships", column: "citizenshipId", primary_key: "citizenshipId", name: "fk_tbl_Applicants_tbl_Citizenships1"
  add_foreign_key "tbl_Applicants", "tbl_CivilStatuses", column: "civilStatusId", primary_key: "civilStatusId", name: "fk_tbl_Applicants_tbl_CivilStatuses1"
  add_foreign_key "tbl_JobApplication_EmploymentRequirements", "tbl_EmploymentRequirements", column: "requirementId", primary_key: "requirementId", name: "fk_tbl_JobApplication_EmploymentRequirements_tbl_EmploymentRe1"
  add_foreign_key "tbl_JobApplication_EmploymentRequirements", "tbl_JobApplications", column: "jobApplicationId", primary_key: "jobApplicationId", name: "fk_tbl_JobApplication_EmploymentRequirements_tbl_JobApplicati1"
  add_foreign_key "tbl_finance_coa", "tbl_finance_account_types", column: "account_id", name: "account_type", on_update: :cascade
  add_foreign_key "tbl_module_access", "tbl_modules", column: "module_id", name: "const_module_access", on_update: :cascade, on_delete: :cascade
  add_foreign_key "tbl_module_access", "tbl_user", column: "userid", name: "const_userid", on_update: :cascade, on_delete: :cascade
end
