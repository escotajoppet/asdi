class CreateShiftAndSchedules < ActiveRecord::Migration[5.1]
  def up
    create_table :tbl_shifts_and_schedules do |t|
    	t.integer		:userid
    	t.string		:start_date, limit: 32
    	t.string		:end_date, limit: 32
    	t.string		:start_time, limit: 24
    	t.string		:end_time, limit: 24
    	t.string		:days

      # t.timestamps
      t.datetime   :created_at, null: false, default: DateTime.now
      t.datetime   :updated_at, null: false, default: DateTime.now
    end
  end

  def down
  	drop_table :tbl_shifts_and_schedules
  end
end
