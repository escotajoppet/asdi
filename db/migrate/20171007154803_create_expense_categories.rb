class CreateExpenseCategories < ActiveRecord::Migration[5.1]
  def up
    create_table :tbl_expense_categories do |t|
    	t.string	:category_name

      t.timestamps
    end
  end

  def down
  	drop_table :tbl_expense_categories
  end
end
