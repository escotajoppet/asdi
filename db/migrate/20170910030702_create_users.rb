class CreateUsers < ActiveRecord::Migration[5.1]
  def up
    create_table :tbl_user do |t|
    	t.string		:username, limit: 15
    	t.string		:password, limit: 32
    	t.datetime	:last_logged_in
    	t.datetime	:last_activity
    	t.string		:user_ip, limit: 32
    	t.boolean		:status_flag
    	t.string		:login_token, limit: 32
      t.boolean   :is_admin

      # t.timestamps
      t.datetime   :created_at, null: false, default: DateTime.now
      t.datetime   :updated_at, null: false, default: DateTime.now
    end
  end

  def down
    drop_table :tbl_user
  end
end
