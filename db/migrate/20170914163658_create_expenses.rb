class CreateExpenses < ActiveRecord::Migration[5.1]
  def up
    create_table :tbl_expenses do |t|
    	t.integer		:cr_id
    	t.string		:expense_date, limit: 32
    	t.text			:description
    	t.string		:category, limit: 64
    	t.float			:cost

      # t.timestamps
      t.datetime   :created_at, null: false, default: DateTime.now
      t.datetime   :updated_at, null: false, default: DateTime.now
    end
  end

  def down
  	drop_table :tbl_expenses
  end
end
