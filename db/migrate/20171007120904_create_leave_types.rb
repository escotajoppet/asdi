class CreateLeaveTypes < ActiveRecord::Migration[5.1]
  def up
    create_table :tbl_leave_types do |t|
    	t.string	:leave_type
    	t.string	:leaves_per_user

      t.timestamps
    end
  end

  def down
  	drop_table :tbl_leave_types
  end
end
