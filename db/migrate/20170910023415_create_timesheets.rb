class CreateTimesheets < ActiveRecord::Migration[5.1]
  def up
    create_table :tbl_timesheets do |t|
    	t.integer    :userid
    	t.date       :date
    	t.datetime   :start_time
    	t.datetime   :end_time
    	t.float      :regular_hours, default: 0
      t.integer    :timesheet_status
    	t.float      :overtime_hours, default: 0
      t.boolean    :overtime_approved, default: false

      # t.timestamps
      t.datetime   :created_at, null: false, default: DateTime.now
      t.datetime   :updated_at, null: false, default: DateTime.now
    end
  end

  def down
    drop_table :tbl_timesheets
  end
end
