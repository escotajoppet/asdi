class CreateClaimAndReimbursements < ActiveRecord::Migration[5.1]
  def up
    create_table :tbl_claims_and_reimbursements do |t|
    	t.integer		:userid
    	t.string		:application_type, limit: 24
    	t.string		:cr_type, limit: 64
    	t.text			:purpose_of_expense
    	t.string		:date_filed, limit: 32
    	t.string		:period_start, limit: 32
    	t.string		:period_end, limit: 32
    	t.string		:cr_status, limit: 24

      # t.timestamps
      t.datetime   :created_at, null: false, default: DateTime.now
      t.datetime   :updated_at, null: false, default: DateTime.now
    end
  end

  def down
  	drop_table :tbl_claims_and_reimbursements
  end
end