class CreateClaimTypes < ActiveRecord::Migration[5.1]
  def up
    create_table :tbl_claim_types do |t|
    	t.string	:claim_type

      t.timestamps
    end
  end

  def down
  	drop_table :tbl_claim_types
  end
end
