class CreateReimbursementTypes < ActiveRecord::Migration[5.1]
  def up
    create_table :tbl_reimbursement_types do |t|
    	t.string	:reimbursement_type

      t.timestamps
    end
  end

  def down
  	drop_table :tbl_reimbursement_types
  end
end
