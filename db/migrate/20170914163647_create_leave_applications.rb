class CreateLeaveApplications < ActiveRecord::Migration[5.1]
  def up
    create_table :tbl_leave_applications do |t|
    	t.integer		:userid
    	t.integer		:leave_type_id
    	t.string		:reason_for_leave
    	t.string		:leave_status, limit: 32
    	t.string		:reason_for_cancelation
    	t.string		:reason_for_disapproval
    	t.string		:leave_filed_date, limit: 32
    	t.string		:leave_start_date, limit: 32
    	t.string		:leave_end_date, limit: 32

      # t.timestamps
      t.datetime   :created_at, null: false, default: DateTime.now
      t.datetime   :updated_at, null: false, default: DateTime.now
    end
  end

  def down
  	drop_table :tbl_leave_applications
  end
end
