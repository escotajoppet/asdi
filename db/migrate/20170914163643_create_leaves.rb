class CreateLeaves < ActiveRecord::Migration[5.1]
  def up
    create_table :tbl_leaves do |t|
    	t.integer		:userid
      t.string    :leave_type_id
      t.integer   :remaining_leaves

      # t.timestamps
      t.datetime   :created_at, null: false, default: DateTime.now
      t.datetime   :updated_at, null: false, default: DateTime.now
    end
  end

  def down
    drop_table :tbl_leaves
  end
end