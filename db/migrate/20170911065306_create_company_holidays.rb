class CreateCompanyHolidays < ActiveRecord::Migration[5.1]
  def up
    create_table :tbl_company_holidays do |t|
    	t.string	:description
    	t.date    :holiday_date

      # t.timestamps
      t.datetime   :created_at, null: false, default: DateTime.now
      t.datetime   :updated_at, null: false, default: DateTime.now
    end
  end

  def down
  	drop_table :tbl_company_holidays
  end
end
