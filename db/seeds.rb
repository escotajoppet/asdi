require 'digest/md5'

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# User.create(username: 'admin', password: Digest::MD5.hexdigest('password'), is_admin: true)
# User.create(username: 'hrismod3', password: Digest::MD5.hexdigest('password'), is_admin: true)
# User.create(username: 'employee', password: Digest::MD5.hexdigest('password'), is_admin: false)

CompanyHoliday.create(holiday_date: '01/11/2017', description: 'All Saints Day')
CompanyHoliday.create(holiday_date: '02/11/2017', description: 'All Souls Day')
CompanyHoliday.create(holiday_date: '30/11/2017', description: 'Bonifacio Day')
CompanyHoliday.create(holiday_date: '25/12/2017', description: 'Christmas Day')
CompanyHoliday.create(holiday_date: '30/12/2017', description: 'Rizal Day')

leave_types = { 
	'Vacation': 15, 
	'Sick': 15, 
	'Service Incentive': 3, 
	'Maternity | Paternity': 7, 
	'Parental': 7, 
	'Rehabilitation': 7, 
	'Study': 7, 
	'Marriage': 7 
}

if LeaveType.all.empty?
	leave_types.each do |k, v| 
		LeaveType.create(leave_type: k.to_s, leaves_per_user: v)
	end
end

claim_types = [
	'Discrimination',
	'Wages',
	'Benefits',
	'Non-competition agreement',
	'Overtime',
	'Workplace discipline',
	'Employment contracts',
	'Unfair dismissal',
]

claim_types.each { |ct| ClaimType.create(claim_type: ct) } if ClaimType.all.empty?

reimbursement_types = [
	'Business Expense',
	'Auto Mileage and Travel',
	'Medical Expense'
]

reimbursement_types.each { |rt| ReimbursementType.create(reimbursement_type: rt) } if ReimbursementType.all.empty?

expense_categories = [
	'Rent',
	'Utilities',
	'Insurance',
	'Fees',
	'Wages',
	'Taxes',
	'Interest',
	'Supplies',
	'Depreciation',
	'Maintenance',
	'Travel',
	'Meals and Entertainment',
	'Training'
]

expense_categories.each { |cn| ExpenseCategory.create(category_name: cn) } if ExpenseCategory.all.empty?