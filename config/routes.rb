Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  root to: 'users#login'

  resources :time_and_attendance, path: 'time-and-attendance' do
  	collection do
      get   'holidays' => 'company_holidays#index'
  	end

    member do
      get   'calendar'
    end
  end

  resources :timesheets do
  	member do
  		post	'time-in-time-out' => 'timesheets#time_in_time_out'
  	end
  end

  resources :company_holidays, path: 'time-and-attendance/holidays' do

  end

  resources :shifts_and_schedules, path: 'shifts-and-schedules' do
    member do
      get   'schedules'
      get   'leave-tracker' => 'shifts_and_schedules#leave_tracker'
    end
  end

  resources :leaves do
    
  end

  resources :leave_types do
    
  end

  resources :claims_and_reimbursements, path: 'claims-and-reimbursements' do
    member do
      get   'expenses'
    end

    collection do
      get   'populate-types' => 'claims_and_reimbursements#populate_types'
    end
  end

  resources :claim_types do
    
  end

  resources :reimbursement_types do
    
  end

  resources :expense_categories do
    
  end

  resources :users do
    collection do
      get   'login'
      get   'logout'
    end

    member do
      post  'authorize'
    end
  end
end
