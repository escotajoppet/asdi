function whenJQueryIsReady() {
	if(typeof $ === 'undefined'){
		setTimeout('whenJQueryIsReady', 250);
	} else{
		$('#menu-claims-and-reimbursements-management').addClass('active');
		$('#menu-reimbursement-types').addClass('active');
	}
}

whenJQueryIsReady();