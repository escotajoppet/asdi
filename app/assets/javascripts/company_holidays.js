function whenJQueryIsReady() {
	if(typeof $ === 'undefined'){
		setTimeout('whenJQueryIsReady', 250);
	} else{
		$('#menu-time-and-attendance').addClass('active');
		$('#menu-company-holidays').addClass('active');	
	}
}

whenJQueryIsReady();