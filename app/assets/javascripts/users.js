function whenJQueryIsReady() {
	if(typeof $ === 'undefined'){
		setTimeout('whenJQueryIsReady', 250);
	} else{
		$('#menu-employees-list').addClass('active');
		$('#menu-human-resource').removeClass('active');
	}
}

whenJQueryIsReady();