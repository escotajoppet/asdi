function whenJQueryIsReady() {
	if(typeof $ === 'undefined'){
		setTimeout('whenJQueryIsReady', 250);
	} else{
		$('#menu-leave-management').addClass('active');	
		$('#menu-leaves').addClass('active');	
	}
}

whenJQueryIsReady();