// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require bootstrap/js/bootstrap.min.js
//= require plugins/daterangepicker/moment.min.js
//= require plugins/daterangepicker/daterangepicker.js
//= require plugins/datepicker/bootstrap-datepicker.js
//= require plugins/fastclick/fastclick.js
//= require plugins/sparkline/jquery.sparkline.min.js
//= require plugins/slimScroll/jquery.slimscroll.min.js
//= require plugins/iCheck/icheck.min.js
//= require dist/js/app.min.js
//= require plugins/fullcalendar/fullcalendar.min.js