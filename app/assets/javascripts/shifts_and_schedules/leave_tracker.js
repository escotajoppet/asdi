function whenJQueryIsReady() {
	if(typeof $ === 'undefined'){
		setTimeout('whenJQueryIsReady', 250);
	} else{
		$('#remaining-leaves-btn').click(function(e){
			e.preventDefault();

			$('#remaining-leaves-modal').modal().show();
		});
	}
}

whenJQueryIsReady();