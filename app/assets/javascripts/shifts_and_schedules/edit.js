function whenJQueryIsReady() {
	if(typeof $ === 'undefined'){
		setTimeout('whenJQueryIsReady', 250);
	} else{
		setSchedules();
		setAddSchedule();
	}
}

function setSchedules(){
	$.ajax({
		url: schedules_shifts_and_schedule_path,
		method: 'GET',
		dataType: 'JSON',
		data: { 'luid': luid },
		success: function(data){
			scheds = data.schedules;

			if(scheds.length > 0){
				for(var i = 0; i < scheds.length; i++){
					addSchedTemplate(scheds[i], i);
				}
			} else{
				addNoSchedMessage();
			}
		}
	})
}

function setAddSchedule(){
	$('#add-schedule-btn').click(function(e){
		e.preventDefault();

		var ids = [];

		$('[data-id]').each(function(){
			ids.push(parseInt($(this).data('id')));
		});

		var maxid = ids.length > 0 ? Math.max.apply(Math, ids) : -1;
		var newid = maxid + 1;

		$('#no-sched-message').remove();

		addSchedTemplate({}, newid);
	});
}

function addNoSchedMessage(){
	sched =		"<table class=\"table table-striped\" id=\"no-sched-message\">";
	sched +=		"<tr>";
	sched +=			"<td align=\"center\">No schedules available</td>";
	sched +=		"</tr>";
	sched +=	"</table>";

	$('#schedules').append(sched);
}

function addSchedTemplate(record, index){
	sched =  "<div class=\"col-md-12\" data-id=\""+ index +"\" style=\"padding: 0 0 10px 0; margin-bottom: 10px; border-bottom: 1px solid #f4f4f4;\">";
	sched += "	<div class=\"col-md-3\">";
	sched += "		<input type=\"text\" name=\"schedules[]\" class=\"form-control schedules"+ index +"\" readonly=\"readonly\" style=\"margin-bottom: 10px;\"/>";

	sched += "		<input type=\"text\" name=\"times[]\" class=\"form-control times"+ index +"\" readonly=\"readonly\">";
	sched += "	</div>";

	sched += "	<div class=\"col-md-8\">";
	sched += "		<table style=\"width: 100%\">";
	sched += "			<tr>";

	for(var i = 0; i < days.length; i++)
		sched +=				"<th><input type=\"checkbox\" name=\"days"+ index +"[]\" value=\""+ days[i] +"\" "+ isChecked(record.days, i) +"> "+ days[i].toUpperCase() +"</th>";

	sched += "			</tr>";
	sched += "		</table>";
	sched += "	</div>";

	sched += "	<div class=\"col-md-1\">";

	sched += "	<a href=\"#\" title=\"Remove Schedule\" class=\"remove-schedule-btn\"><i class=\"fa fa-close\"></i></a>";

	sched += "	</div>";
	sched += "</div>";

	$('#schedules').append(sched);

	setPickers(record, index);
	setDeleteSchedule();
}

function setPickers(record, index){
	var config = {
    'timePicker': true,
    'showDropdowns': true,
 		'autoApply': true,
 		'alwaysShowCalendars': true,
 		// 'minDate': moment(),
 		'locale': {
      format: 'MM/DD/YYYY'
    }
	}

	if(typeof record !== 'undefined'){
		config.startDate = record.start_date;
    config.endDate = record.end_date;
	}

	$(".schedules"+ index).daterangepicker(config);

	var picker = $(".schedules"+ index).data('daterangepicker');

	if(typeof record.id !== 'undefined'){
		$(".times"+ index).val(record.start_time +" - "+ record.end_time);
	} else{
		setTimes(picker, index);
	}

	$(".schedules"+ index).on('apply.daterangepicker', function(ev, picker) {
		setTimes(picker, index);
	});
	
	$('[name=daterangepicker_start], [name=daterangepicker_end]').attr('readonly', 'readonly');
}

function setTimes(picker, id){
	$(".times"+ id).val(picker.startDate.format('h:mm A') +" - "+ picker.endDate.format('h:mm A'));
}

function setDeleteSchedule(){
	$('.remove-schedule-btn').off('click').on('click', function(e){
		e.preventDefault();

		$(this).parent().parent().remove();
	});
}

function isChecked(schedays, i){
	if(typeof schedays !== 'undefined'){
		if(schedays.includes(i + 1)) return 'checked';
	}
}

whenJQueryIsReady();