function whenJQueryIsReady() {
	if(typeof $ === 'undefined'){
		setTimeout('whenJQueryIsReady', 250);
	} else{
		$('#menu-shifts-and-schedules').addClass('active');
	}
}

whenJQueryIsReady();