function whenJQueryIsReady() {
	if(typeof $ === 'undefined'){
		setTimeout('whenJQueryIsReady', 250);
	} else{
		populateApplicationType();
		setDatePickers();
		setAddItemBtn();
		setCreateItemBtn();
	}
}

function populateApplicationType(){
	populateTypes($('#application-type').val());

	$('#application-type').change(function(e){
		populateTypes($(this).val());
	});
}

function populateTypes(app_type){
	$.ajax({
		url: populate_types_claims_and_reimbursements_path,
		method: 'GET',
		dataType: 'SCRIPT',
		data: { 'apptype': app_type, 'luid': luid }
	});
}

function setDatePickers(){
	$('#period-start, #period-end, [name=expense_date]').datepicker();
}

function setAddItemBtn(){
	$('#add-item-btn').click(function(e){
		e.preventDefault();

		$('#add-item-modal').modal('show');
	});
}

function setCreateItemBtn(){
	$('#create-item-btn').click(function(e){
		e.preventDefault();

		var ed = $('[name=expense_date]').val();
		var d = $('[name=description]').val();
		var ca = $('[name=category]').val();
		var co = $('[name=cost]').val();

		record = 	"<tr class=\"expenses\">";
		record +=		"<td>"+ ed +"</td>";
		record +=		"<td>"+ d +"</td>";
		record +=		"<td>"+ ca +"</td>";
		record +=		"<td>"+ co +"</td>";
		record +=		"<td><a href=\"#\" data-remote=\"true\" data-toggle=\"tooltip\" data-confirm=\"Delete expense?\" title=\"Delete\"><i class=\"fa fa-close delete-item\"></i></a></td>"
		record += "</tr>";

		$('tbody').append(record);

		setDeleteItem();

		$('[name=expense_date]').val('');
		$('[name=description]').val('');
		$('[name=category]').val('');
		$('[name=cost]').val('');

		append_input('expense_date', ed);
		append_input('description', d);
		append_input('category', ca);
		append_input('cost', co);

		$('#add-item-modal').modal('hide');
	});
}

function setDeleteItem(){
	$('.delete-item').off('click').on('click', function(e){
		e.preventDefault();

		var $record = $(this).closest('tr');

		$record.remove();
	})
}

function append_input(name, value){
	$('#hidden-fields').append("<input type=\"hidden\" name=\""+ name +"[]\" value=\""+ value +"\"/>");
}

whenJQueryIsReady();