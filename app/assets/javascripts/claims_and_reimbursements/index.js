function whenJQueryIsReady() {
	if(typeof $ === 'undefined'){
		setTimeout('whenJQueryIsReady', 250);
	} else{
		$('#expenses-modal').on('hidden.bs.modal', function(e){
			$('#expenses-list').html('');	
		});
	}
}

whenJQueryIsReady();