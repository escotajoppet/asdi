function whenJQueryIsReady() {
	if(typeof $ === 'undefined'){
		setTimeout('whenJQueryIsReady', 250);
	} else{
		$('#menu-calendar').addClass('active');	

		var events = [];

		for(var key in holidays) {
		  if (holidays.hasOwnProperty(key)){
		  	events.push({
		  		title: holidays[key],
		  		start: key,
		  		end: key,
		  		allDay: true,
		  		color: 'black'
		  	});
		  }
		}

		for(var i = 0; i < leaves.length; i++){
			console.log(leaves[i]);

			events.push({
				title: leaves[i]['type'] +" - "+ leaves[i]['status'].toUpperCase(),
				start: leaves[i]['sdate'],
				end: leaves[i]['edate'],
				allDay: true,
				color: leaves[i]['color']
			});
		}

		for (var i = 0; i < attendance.length; i++) {
			events.push({
	  		title: attendance[i]['status'].toUpperCase(),
	  		start: attendance[i]['date'],
	  		end: attendance[i]['date'],
	  		allDay: true,
	  		color: attendance[i]['color']
	  	});
		};

		$('#calendar').fullCalendar({
			events: events
		});
	}
}

whenJQueryIsReady();