function whenJQueryIsReady() {
	if(typeof $ === 'undefined'){
		setTimeout('whenJQueryIsReady', 250);
	} else{
		$('#menu-claims-and-reimbursements-management').addClass('active');
		$('#menu-expense-categories').addClass('active');
	}
}

whenJQueryIsReady();