function whenJQueryIsReady() {
	if(typeof $ === 'undefined'){
		setTimeout('whenJQueryIsReady', 250);
	} else{
		$('#menu-timesheets').addClass('active');	
	}
}

whenJQueryIsReady();