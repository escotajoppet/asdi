function whenJQueryIsReady() {
	if(typeof $ === 'undefined'){
		setTimeout('whenJQueryIsReady', 250);
	} else{
		$('#company_holiday_holiday_date').datepicker({
			format: 'dd/mm/yyyy'
		});
	}
}

whenJQueryIsReady();