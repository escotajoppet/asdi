function whenJQueryIsReady() {
	if(typeof $ === 'undefined'){
		setTimeout('whenJQueryIsReady', 250);
	} else{
		setDatePickers();
	}
}

function setDatePickers(){
	$('#start_date, #end_date').datepicker('setDaysOfWeekDisabled', '06');
}

whenJQueryIsReady();