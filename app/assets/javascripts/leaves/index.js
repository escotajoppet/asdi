function whenJQueryIsReady() {
	if(typeof $ === 'undefined'){
		setTimeout('whenJQueryIsReady', 250);
	} else{
		setApprovalsOnClick();
	}
}

function setApprovalsOnClick(){
	$('.reason-modal').click(function(e){
		e.preventDefault();

		var word = $(this).data('status') === 'canceled' ? 'cancelation' : 'disapproval';

		$('.what').html(word);
		$('#reason-modal-body').html($(this).data('reason'));

		$('#reason-modal').modal('show');
	});

	$('.reason-form-btn').click(function(e){
		e.preventDefault();

		var $btn = $(this);

		var word = $btn.data('action') === 'cancel' ? 'cancelation' : 'disapproval';

		$('.what').html(word);
		$('#reason').attr('placeholder', 'Reason for '+ word);

		$('#reason-form').attr('action', "/leaves/"+ $btn.data('id'));
		$('#uaction').val($btn.data('action'));

		$('#reason-form-modal').modal('show');
	})
}

whenJQueryIsReady();