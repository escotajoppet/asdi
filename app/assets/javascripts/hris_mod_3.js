function whenJQueryIsReady() {
	if(typeof $ === 'undefined'){
		setTimeout('whenJQueryIsReady', 250);
	} else{
		$('#menu-human-resource').addClass('active');

		fadeOutFlash();
	}
}

function fadeOutFlash(){
	$('#flash').delay(500).fadeIn('normal', function() {
    $(this).delay(2500).fadeOut();
  });
}

whenJQueryIsReady();