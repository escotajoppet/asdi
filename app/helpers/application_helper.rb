module ApplicationHelper
	def current_user
		User.find(params[:luid])
	end

	def uaction
		return current_user.is_admin ? 'Manage' : 'View'
	end

	def format_time(time)
		time.nil? ? 'n/a' : time.strftime('%H:%M:%S')
	end

	def display_alert
		return flash[:notice] if flash[:notice].present?
		return flash[:alert] if flash[:alert].present?
	end

	def message_type
		flash[:notice].present? ? 'Success' : 'Error'
	end

	def alert_type
		flash[:notice].present? ? 'success' : 'danger'
	end

	def alert_icon
		flash[:notice].present? ? 'check' : 'ban'
	end
end