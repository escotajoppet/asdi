module ClaimsAndReimbursementsHelper
	def populate_types
		types = nil
		types = options_for_select(ClaimType.all.map{ |ct| [ct.claim_type, ct.claim_type]}, nil) if params[:apptype].eql?('Claim')
		types = options_for_select(ReimbursementType.all.map{ |rt| [rt.reimbursement_type, rt.reimbursement_type]}, nil) if params[:apptype].eql?('Reimbursement')

		types
	end
end
