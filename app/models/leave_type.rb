class LeaveType < ApplicationRecord
	self.table_name = 'tbl_leave_types'

	has_many	:leaves,
						class_name: 'Leave',
						foreign_key: :leave_type_id,
						dependent: :delete_all

	has_many	:leave_applications,
						class_name: 'LeaveApplication',
						foreign_key: :leave_type_id,
						dependent: :nullify

	after_create :grant_leave_to_users

	def leave_type=(leave_type)
		write_attribute(:leave_type, leave_type.titleize)
	end

	def leaves_per_user=(leaves)
		write_attribute(:leaves_per_user, leaves.to_i)
	end

	private

		def grant_leave_to_users
			User.all.each do |user|
				Leave.create(leave_type: self, user: user, remaining_leaves: self.leaves_per_user)
			end
		end
end
