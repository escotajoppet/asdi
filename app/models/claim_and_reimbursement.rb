class ClaimAndReimbursement < ApplicationRecord
	self.table_name = 'tbl_claims_and_reimbursements'

	has_many		:expenses,
							class_name: 'Expense',
							foreign_key: :cr_id,
							dependent: :delete_all

	belongs_to	:user,
							class_name: 'User',
							foreign_key: :userid

	def total_expenses
		self.expenses.sum(:cost)
	end
end
