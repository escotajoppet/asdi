class Expense < ApplicationRecord
	self.table_name = 'tbl_expenses'

	belongs_to		:claim_reimbursement,
								class_name: 'ClaimAndReimbusement',
								foreign_key: :cr_id
end
