class ReimbursementType < ApplicationRecord
	self.table_name = 'tbl_reimbursement_types'

	def reimbursement_type=(reimbursement_type)
		write_attribute(:reimbursement_type, reimbursement_type.titleize)
	end
end
