class ClaimType < ApplicationRecord
	self.table_name = 'tbl_claim_types'

	def claim_type=(claim_type)
		write_attribute(:claim_type, claim_type.titleize)
	end
end
