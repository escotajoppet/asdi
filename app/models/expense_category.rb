class ExpenseCategory < ApplicationRecord
	self.table_name = 'tbl_expense_categories'

	def category_name=(category_name)
		write_attribute(:category_name, category_name.titleize)
	end
end
