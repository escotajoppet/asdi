class CompanyHoliday < ApplicationRecord
	self.table_name = 'tbl_company_holidays'

	validates_presence_of :description, :holiday_date
	validates_uniqueness_of :holiday_date

	def holiday_date=(date)
		write_attribute(:holiday_date, Date.parse(date))
	end
end
