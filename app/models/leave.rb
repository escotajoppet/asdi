class Leave < ApplicationRecord
	self.table_name = 'tbl_leaves'

	belongs_to		:user,
								class_name: 'User',
								foreign_key: :userid

	belongs_to		:leave_type,
								class_name: 'LeaveType',
								foreign_key: :leave_type_id
end
