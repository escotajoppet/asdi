class Timesheet < ApplicationRecord
	self.table_name = 'tbl_timesheets'

	belongs_to	:user,
							class_name: 'User',
							foreign_key: :userid

	scope :for_timeout, -> { where.not(start_time: nil).where(end_time: nil) }

	enum timesheet_status: { fulfilled: 0, undertime: 1, overtime: 2 }, _suffix: true

	def compute_regular_hours
		required_hours = self.user.required_hours # get data from shift_and_schedules part

		# required_hours = 9 # assumed value

		diff = time_diff

		self.regular_hours = diff.round(2)
		self.regular_hours = required_hours if diff >= required_hours
	end

	def compute_overtime_hours
		required_hours = self.user.required_hours # get data from shift_and_schedules part

		# required_hours = 9 # assumed value

		diff = time_diff

		excess = diff - required_hours

		if diff < required_hours
			self.timesheet_status = :undertime
		else
			if excess > 1
				self.overtime_hours = excess.round(2)
				self.timesheet_status = :overtime
			else
				self.timesheet_status = :fulfilled
			end
		end
	end

	def total_hours
		self.regular_hours + self.overtime_hours
	end

	private

		def time_diff
			(self.end_time.to_f - self.start_time.to_f) / 3600
		end
end
