class User < ApplicationRecord
	self.table_name = 'tbl_user'
	self.primary_key = 'id'

	has_many 	:timesheets,
						class_name: 'Timesheet',
						foreign_key: :userid

	has_many	:leave_applications,
						class_name: 'LeaveApplication',
						foreign_key: :userid

	has_many	:leaves,
						class_name: 'Leave',
						foreign_key: :userid

	has_many	:claims_and_reimbursements,
						class_name: 'ClaimAndReimbursement',
						foreign_key: :userid

	has_many	:schedules,
						class_name: 'ShiftAndSchedule',
						foreign_key: :userid

	def required_hours
		totime = Time.now
		todate = totime.to_date

		stime = self.active_schedule.start_time.to_time
		etime = self.active_schedule.end_time.to_time
		smeridies = meridies(stime)
		emeridies = meridies(etime)

		diff = 0

		diff = smeridies.eql?('pm') && emeridies.eql?('am') ? ((etime.to_datetime + 1).to_time - stime) : etime - stime
		diff /= 3600

		diff
	end

	def active_schedule
		active = nil
		todate = Time.now.to_date

		self.schedules.each do |schedule|
			sdatetime = format_schedule(schedule.start_date, schedule.start_time)
			edatetime = format_schedule(schedule.end_date, schedule.end_time)

			if todate.between?(sdatetime.to_date, edatetime.to_date)
				active = schedule
				break
			end
		end

		active
	end

	private

		def format_schedule(date, time) # from MM/DD/YYYY to DD/MM/YYYY
			tmp = date.split('/')

			ndate = "#{tmp[1]}/#{tmp.first}/#{tmp.last} #{time}"

			ndate.to_datetime
		end

		def meridies(time)
			time.strftime("%P")
		end
end