class ShiftAndSchedule < ApplicationRecord
	self.table_name = 'tbl_shifts_and_schedules'

	belongs_to	:user,
							class_name: 'User',
							foreign_key: :userid

	def onduty_days
		self.days.split('').map { |day| adays[day.to_sym].titleize }.join(', ')
	end

	def offduty_days
		tdays = Array.new
		odd = self.days.split('')

		adays.each { |k, v| tdays << k.to_s unless odd.include?(k.to_s) }
		tdays.map { |day| adays[day.to_sym].titleize }.join(', ')
	end
end
