class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  # protect_from_forgery with: :null_session

  private

  	def authenticate_user!
  		if params[:luid].nil?
  			redirect_to login_users_path
  		else
  			user = User.find_by_id(params[:luid])

  			if user.nil?
  				redirect_to login_users_path
  			else
          # redirect_to login_users_path if user.login_token.nil?
  				redirect_to login_users_path if user.login_tokken.nil?
  			end
  		end
  	end

  	def current_user
			User.find(params[:luid])
		end
end
