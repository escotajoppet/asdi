class TimeAndAttendanceController < ApplicationController
	before_action :authenticate_user!

	def calendar
		user = User.find(params[:id])

		@holidays = Hash.new

		CompanyHoliday.all.each { |holiday| @holidays["#{holiday.holiday_date.to_s}"] = holiday.description }

		@attendance = Array.new

		user.timesheets.each do |timesheet|
			next if timesheet.timesheet_status.nil?

			color = ''

			case timesheet.timesheet_status
				when 'fulfilled'
					color = 'lightgreen'
				when 'undertime'
					color = 'lightred'
				when 'overtime'
					color = 'lightblue'
			end

			@attendance << {
				date: timesheet.date.to_s,
				status: timesheet.timesheet_status,
				color: color
			}
		end

		@leaves = Array.new

		user.leave_applications.each do |leave|
			color = ''

			case leave.leave_status.downcase
				when 'pending'
					color = 'blue'
				when 'approved'
					color = 'green'
				when 'disapproved'
					color = 'red'
				when 'canceled'
					color = 'orange'
			end

			@leaves << {
				sdate: leave.leave_start_date,
				edate: leave.leave_end_date,
				type: leave.leave_type.nil? ? 'N/A' : leave.leave_type.leave_type,
				status: leave.leave_status,
				color: color
			}
		end
	end
end
