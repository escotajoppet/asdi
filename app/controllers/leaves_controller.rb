class LeavesController < ApplicationController
	before_action :authenticate_user!

	def index
		respond_to do |format|
			format.html do
				@leaves = current_user.is_admin ? LeaveApplication.all : current_user.leave_applications
			end
		end
	end

	def new
		respond_to do |format|
			format.html do
				@leave = LeaveApplication.new
			end
		end
	end

	def create
		respond_to do |format|
			format.html do
				uri = URI('http://ec2-54-200-53-10.us-west-2.compute.amazonaws.com:8080/asdi/leaves/file')

				lp = params[:leave]

				req = Net::HTTP::Post.new(uri)
				req.body = URI.encode_www_form({
					user_id: params[:luid],
					leave_type_id: lp[:leave_type],
					reason_for_leave: lp[:reason_for_leave],
					start_date: lp[:start_date],
					end_date: lp[:end_date]
				})

				res = Net::HTTP.start(uri.hostname, uri.port) { |http| http.request(req) }

				resp = JSON.parse(res.body)

				if resp['responseCode'].eql?(200)
					flash[:notice] = 'Leave has been successfully filed'
					redirect_to leaves_path(luid: params[:luid])
				else
					flash[:alert] = resp['response']
					redirect_to new_leafe_path(luid: params[:luid])
				end
			end
		end
	end

	def update
		respond_to do |format|
			format.html do
				leave = LeaveApplication.find(params[:id])
				user = leave.user

				uri = URI('http://ec2-54-200-53-10.us-west-2.compute.amazonaws.com:8080/asdi/leaves/manage')

				req = Net::HTTP::Post.new(uri)
				req.body = URI.encode_www_form({
					user_id: user.id,
					leave_id: leave.id,
					reason: params[:reason],
					action: params[:uaction]
				})

				res = Net::HTTP.start(uri.hostname, uri.port) { |http| http.request(req) }

				resp = JSON.parse(res.body)

				if resp['responseCode'].eql?(200)
					flash[:notice] = "Leave has been successfully #{params[:uaction].downcase.eql?('cancel') ? "canceled" : "#{params[:uaction]}d"}"
				else
					flash[:alert] = resp['response']
				end
				
				redirect_to leaves_path(luid: params[:luid])
			end
		end
	end
end
