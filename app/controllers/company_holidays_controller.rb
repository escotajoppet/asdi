class CompanyHolidaysController < ApplicationController
	before_action :authenticate_user!
	before_action :holidays_list, only: [:index, :destroy]
	before_action :holiday_mod, only: [:edit, :update, :destroy]

	def index
		respond_to { |format| format.html }
	end

	def new
		respond_to { |format| format.html { @holiday = CompanyHoliday.new } }
	end

	def create
		respond_to do |format|
			format.html do
				@holiday = CompanyHoliday.new(company_holiday_params)

				if @holiday.save
					flash[:notice] = 'Company holiday has been successfully created'
					redirect_to company_holidays_path(luid: params[:luid])
				else
					flash[:alert] = @holiday.errors.full_messages
					redirect_to new_company_holiday_path(luid: params[:luid])
				end
			end
		end
	end

	def edit
		respond_to { |format| format.html }
	end

	def update
		respond_to do |format|
			format.html do
				if @holiday.update_attributes(company_holiday_params)
					flash[:notice] = 'Company holiday has been successfully updated'
					redirect_to company_holidays_path(luid: params[:luid])
				else
					flash[:alert] = @holiday.errors.full_messages
					redirect_to edit_company_holiday_path(@holiday, luid: params[:luid])
				end
			end
		end
	end

	def destroy
		respond_to do |format|
			format.js do
				@holiday.destroy
				flash[:notice] = 'Company holiday has been successfully deleted.'
			end
		end
	end

	private

		def company_holiday_params
			params.require(:company_holiday).permit(:holiday_date, :description)
		end

		def holidays_list
			@holidays = CompanyHoliday.all
		end

		def holiday_mod
			@holiday = CompanyHoliday.find(params[:id])
		end
end
