class LeaveTypesController < ApplicationController
	before_action :authenticate_user!
	before_action :leave_types_list, only: [:index, :destroy]
	before_action :leave_type_mod, only: [:edit, :update, :destroy]

	def index
		respond_to do |format|
			format.html
		end
	end

	def new
		respond_to do |format|
			format.html do
				@leave_type = LeaveType.new
			end
		end
	end

	def create
		respond_to do |format|
			format.html do
				@leave_type = LeaveType.new(leave_type_params)

				if @leave_type.save
					flash[:notice] = 'Leave type has been successfully created'
					redirect_to leave_types_path(luid: params[:luid])
				else
					flash[:alert] = @leave_type.errors.full_messages
					redirect_to new_leave_type_path(luid: params[:luid])
				end
			end
		end
	end

	def edit
		respond_to do |format|
			format.html
		end
	end

	def update
		respond_to do |format|
			format.html do
				if @leave_type.update_attributes(leave_type_params)
					flash[:notice] = 'Leave type has been successfully updated'
					redirect_to leave_types_path(luid: params[:luid])
				else
					flash[:alert] = @leave_type.errors.full_messages
					redirect_to edit_leave_type_path(@leave_type, luid: params[:luid])
				end
			end
		end
	end

	def destroy
		respond_to do |format|
			format.js do
				flash[:notice] = 'Leave type has been successfully deleted'
				@leave_type.destroy
			end
		end
	end

	private

		def leave_type_params
			params.require(:leave_type).permit(:leave_type, :leaves_per_user)
		end

		def leave_types_list
			@leave_types = LeaveType.all
		end

		def leave_type_mod
			@leave_type = LeaveType.find(params[:id])
		end
end
