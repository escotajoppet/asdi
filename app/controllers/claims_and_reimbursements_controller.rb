class ClaimsAndReimbursementsController < ApplicationController
	before_action :authenticate_user!

	def index
		respond_to do |format|
			format.html do
				@crs = current_user.is_admin ? ClaimAndReimbursement.all : current_user.claims_and_reimbursements
			end
		end
	end

	def new
		respond_to do |format|
			format.html do
				@claim_and_reimbursement = ClaimAndReimbursement.new
			end
		end
	end

	def create
		respond_to do |format|
			format.html do 
				uri = URI('http://ec2-54-200-53-10.us-west-2.compute.amazonaws.com:8080/asdi/claims_and_reimbursement/file')

				crp = params[:claim_and_reimbursement]

				req = Net::HTTP::Post.new(uri)
				req.body = URI.encode_www_form({
					user_id: params[:luid],
					application_type: crp[:application_type],
					type: crp[:type],
					purpose_of_expense: crp[:purpose_of_expense],
					period_start: crp[:period_start],
					period_end: crp[:period_end],
					expense_date: params[:expense_date],
					description: params[:description],
					category: params[:category],
					cost: params[:cost]
				})

				res = Net::HTTP.start(uri.hostname, uri.port) { |http| http.request(req) }

				resp = JSON.parse(res.body)

				if resp['responseCode'].eql?(200)
					flash[:notice] = "#{crp[:application_type].titleize} has been successfully filed"
					redirect_to claims_and_reimbursements_path(luid: params[:luid])
				else
					flash[:alert] = resp['response']
					redirect_to new_claims_and_reimbursement_path(luid: params[:luid])
				end
			end
		end
	end

	def update
		respond_to do |format|
			format.html do
				uri = URI('http://ec2-54-200-53-10.us-west-2.compute.amazonaws.com:8080/asdi/claims_and_reimbursement/manage')
				cr = ClaimAndReimbursement.find(params[:id])

				req = Net::HTTP::Post.new(uri)
				req.body = URI.encode_www_form({
					cr_id: params[:id],
					status: params[:uaction].upcase
				})

				res = Net::HTTP.start(uri.hostname, uri.port) { |http| http.request(req) }

				resp = JSON.parse(res.body)

				if resp['responseCode'].eql?(200)
					flash[:notice] = "#{cr.application_type.titleize} has been successfully #{params[:uaction].downcase}d"
				else
					flash[:alert] = resp['response']
				end

				redirect_to claims_and_reimbursements_path(luid: params[:luid])
			end
		end
	end

	def expenses
		respond_to do |format|
			format.js do
				claim_and_reimbursement = ClaimAndReimbursement.find(params[:id])
				@expenses = claim_and_reimbursement.expenses
				@total_expenses = claim_and_reimbursement.total_expenses
			end
		end
	end

	def populate_types
		respond_to do |format|
			format.js
		end
	end
end
