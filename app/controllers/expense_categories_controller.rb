class ExpenseCategoriesController < ApplicationController
	before_action :authenticate_user!
	before_action :expense_categories_list, only: [:index, :destroy]
	before_action :expense_category_mod, only: [:edit, :update, :destroy]

	def index
		respond_to do |format|
			format.html
		end
	end

	def new
		respond_to do |format|
			format.html do
				@expense_category = ExpenseCategory.new
			end
		end
	end

	def create
		respond_to do |format|
			format.html do
				@expense_category = ExpenseCategory.new(expense_category_params)

				if @expense_category.save
					flash[:notice] = 'Expense category has been successfully created'
					redirect_to expense_categories_path(luid: params[:luid])
				else
					flash[:alert] = @expense_category.errors.full_messages
					redirect_to new_expense_category_path(luid: params[:luid])
				end
			end
		end
	end

	def edit
		respond_to do |format|
			format.html
		end
	end

	def update
		respond_to do |format|
			format.html do
				if @expense_category.update_attributes(expense_category_params)
					flash[:notice] = 'Expense category has been successfully updated'
					redirect_to expense_categories_path(luid: params[:luid])
				else
					flash[:alert] = @expense_category.errors.full_messages
					redirect_to edit_expense_category_path(@expense_category, luid: params[:luid])
				end
			end
		end
	end

	def destroy
		respond_to do |format|
			format.js do
				@expense_category.destroy
				flash[:notice] = 'Expense category has been successfully deleted'
			end
		end
	end

	private

		def expense_category_params
			params.require(:expense_category).permit(:category_name)
		end

		def expense_categories_list
			@expense_categories = ExpenseCategory.all
		end

		def expense_category_mod
			@expense_category = ExpenseCategory.find(params[:id])
		end
end
