class ShiftsAndSchedulesController < ApplicationController
	before_action :authenticate_user!
	before_action :user_mod, only: [:edit, :update, :show, :leave_tracker, :schedules]

	def index
		respond_to do |format|
			format.html
		end
	end

	def show
		respond_to do |format|
			format.html
		end
	end

	def edit
		respond_to do |format| 
			format.html do
				@schedules = current_user.schedules
			end
		end
	end

	def update
		respond_to do |format|
			format.html do
				uri = URI('http://ec2-54-200-53-10.us-west-2.compute.amazonaws.com:8080/asdi/shifts_and_schedules/file')

				req = Net::HTTP::Post.new(uri)

				reqbody = {
					id: @user.id,
					schedules: params[:schedules],
					times: params[:times]	
				}

				i = 0

				while 1 do
					break if params["days#{i}".to_sym].nil?

					reqbody["day#{i}".to_sym] = params["days#{i}".to_sym]

					i += 1
				end

				req.body = URI.encode(reqbody.map{ |k, v| "#{k}=#{v}" }.join("&"))

				res = Net::HTTP.start(uri.hostname, uri.port) { |http| http.request(req) }

				resp = JSON.parse(res.body)

				if resp['status'].downcase.eql?('success')
					flash[:notice] = "#{@user.username}'s schedules has been successfully updated"
					redirect_to shifts_and_schedule_path(@user, luid: params[:luid])
				else
					flash[:alert] = 'Schedule required.'
					redirect_to edit_shifts_and_schedule_path(@user, luid: params[:luid])
				end
			end
		end
	end

	def schedules
		respond_to do |format|
			format.json do
				scheds = Array.new

				@user.schedules.each do |schedule|
					scheds << {
						id: schedule.id,
						start_date: schedule.start_date,
						end_date: schedule.end_date,
						start_time: schedule.start_time,
						end_time: schedule.end_time,
						days: schedule.days,
					}
				end

				render json: { schedules: scheds }
			end
		end
	end

	def leave_tracker
		respond_to do |format|
			format.html do
				@leaves = @user.leaves
			end
		end
	end

	private

		def user_mod
			@user = User.find(params[:id])
		end
end
