class ReimbursementTypesController < ApplicationController
	before_action :authenticate_user!
	before_action :reimbursement_types_list, only: [:index, :destroy]
	before_action :reimbursement_type_mod, only: [:edit, :update, :destroy]

	def index
		respond_to do |format|
			format.html
		end
	end

	def new
		respond_to do |format|
			format.html do
				@reimbursement_type = ReimbursementType.new
			end
		end
	end

	def create
		respond_to do |format|
			format.html do
				@reimbursement_type = ReimbursementType.new(reimbursement_type_params)

				if @reimbursement_type.save
					flash[:notice] = 'Reimbursement type has been successfully created'
					redirect_to reimbursement_types_path(luid: params[:luid])
				else
					flash[:alert] = @reimbursement_type.errors.full_messages
					redirect_to new_reimbursement_type_path(luid: params[:luid])
				end
			end
		end
	end

	def edit
		respond_to do |format|
			format.html
		end
	end

	def update
		respond_to do |format|
			format.html do
				if @reimbursement_type.update_attributes(reimbursement_type_params)
					flash[:notice] = 'Reimbursement type has been successfully updated'
					redirect_to reimbursement_types_path(luid: params[:luid])
				else
					flash[:alert] = @reimbursement_type.errors.full_messages
					redirect_to edit_reimbursement_type_path(@reimbursement_type, luid: params[:luid])
				end
			end
		end
	end

	def destroy
		respond_to do |format|
			format.js do
				@reimbursement_type.destroy
				flash[:notice] = 'Reimbursement type has been successfully deleted'
			end
		end
	end

	private

		def reimbursement_type_params
			params.require(:reimbursement_type).permit(:reimbursement_type)
		end

		def reimbursement_types_list
			@reimbursement_types = ReimbursementType.all
		end

		def reimbursement_type_mod
			@reimbursement_type = ReimbursementType.find(params[:id])
		end
end