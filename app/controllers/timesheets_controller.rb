class TimesheetsController < ApplicationController
	before_action :authenticate_user!

	def index
		respond_to do |format|
			format.html do
				@action = current_user.timesheets.for_timeout.take.nil? ? 'in' : 'out'
			end
		end
	end

	def time_in_time_out
		respond_to do |format|
			format.js do
				if !current_user.schedules.empty? && !current_user.active_schedule.nil?
					timesheet = current_user.timesheets.for_timeout.take

					timenow = Time.now
					todate = timenow.to_date

					unless timesheet.nil?
						timesheet.end_time = timenow
						timesheet.compute_regular_hours
						timesheet.compute_overtime_hours
					else
						timesheet = Timesheet.new
						timesheet.date = todate
						timesheet.user = current_user
						timesheet.start_time = timenow
					end

					timesheet.save

					@action = current_user.timesheets.for_timeout.take.nil? ? 'in' : 'out'

					flash[:notice] = "#{current_user.username} has successfully timed #{@action.eql?('in') ? 'out' : 'in'}"
				else
					flash[:alert] = "#{current_user.username} does not have any active schedules"
				end
			end
		end
	end
end
