class ClaimTypesController < ApplicationController
	before_action :authenticate_user!
	before_action :claim_types_list, only: [:index, :destroy]
	before_action :claim_type_mod, only: [:edit, :update, :destroy]

	def index
		respond_to do |format|
			format.html
		end
	end

	def new
		respond_to do |format|
			format.html do
				@claim_type = ClaimType.new
			end
		end
	end

	def create
		respond_to do |format|
			format.html do
				@claim_type = ClaimType.new(claim_type_params)

				if @claim_type.save
					flash[:notice] = 'Claim type has been successfully created'
					redirect_to claim_types_path(luid: params[:luid])
				else
					redirect_to new_claim_type_path(luid: params[:luid])
				end
			end
		end
	end

	def edit
		respond_to do |format|
			format.html
		end
	end

	def update
		respond_to do |format|
			format.html do
				if @claim_type.update_attributes(claim_type_params)
					flash[:notice] = 'Claim type has been successfully updated'
					redirect_to claim_types_path(luid: params[:luid])
				else
					redirect_to edit_claim_type_path(@claim_type, luid: params[:luid])
				end
			end
		end
	end

	def destroy
		respond_to do |format|
			format.js do
				@claim_type.destroy
				flash[:notice] = 'Claim type has been successfully deleted'
			end
		end
	end

	private

		def claim_type_params
			params.require(:claim_type).permit(:claim_type)
		end

		def claim_types_list
			@claim_types = ClaimType.all
		end

		def claim_type_mod
			@claim_type = ClaimType.find(params[:id])
		end
end
