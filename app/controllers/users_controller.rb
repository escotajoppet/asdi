class UsersController < ApplicationController
	layout false, only: [:login, :authorize, :logout]

	before_action :authenticate_user!, only: [:index]

	def index
		respond_to do |format|
			format.html do
				@users = current_user.is_admin ? User.all : User.where(id: current_user.id)
			end
		end
	end

	def login
		respond_to do |format|
			format.html do
				redirect_to timesheets_path(luid: params[:luid]) unless params[:luid].nil?
			end
		end
	end

	def authorize
		respond_to do |format|
			format.js do
				user = User.find_by_id(params[:userid])
				# user.update_attributes({ login_token: Digest::MD5.hexdigest(Time.now.to_s) }) # tmp

				if params[:code].eql?('200')
					flash[:notice] = 'Logged in successfully'
					render js: "window.location.href='#{timesheets_path(luid: params[:userid])}';"
				else
					flash[:alert] = params[:msg]
					render js: "window.location.href='#{login_users_path}';"
				end
			end
		end
	end

	def logout
		respond_to do |format|
			format.html do
				# User.find(params[:luid]).update_attributes(login_token: nil)
				User.find(params[:luid]).update_attributes(login_tokken: nil)

				flash[:notice] = 'Logged out successfully'
				redirect_to login_users_path
			end
		end
	end
end
